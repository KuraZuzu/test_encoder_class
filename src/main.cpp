#include "stm32f4xx.h"
#include <mbed.h>
#include "original_encoder/original_encoder_ab.h"
#include "original_encoder/original_encoder_a.h"
#include "nucleo_rotary_encoder/rotary_encoder_ab_phase.hpp"
#include "nucleo_rotary_encoder/rotary_encoder_a_phase.hpp"

Serial pc(USBTX, USBRX);
// rotary_encoder_a_phase e(TIM2, 100);
// OriginalEncoderAb e(PA_0, PA_1);
OriginalEncoderA e(PC_2, PC_3);

int main() {
    pc.baud(9600);
    e.start();
    e.reset();

    while(1) {
        // printf("%d, %f\n", e.get_counts(), e.get_revol_num());
        printf("%d, %d\n",e.get_cw(), e.get_pulse_cnt() );
        wait(0.2);
    }

}
