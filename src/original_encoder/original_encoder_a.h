#ifndef INCLUDED_ORIGINAL_ENCODER_A
#define INCLUDED_ORIGINAL_ENCODER_A

#include "mbed.h"

class OriginalEncoderA {

private:
    InterruptIn _a;
    DigitalIn _b;
    bool cw; //回転方向 true<-正転、false<-逆転
    int pulse_cnt = 0; //回転数

public:

    OriginalEncoderA(PinName a_pin, PinName b_pin) : _a(a_pin), _b(b_pin), pulse_cnt(0){

    }

    void start(){
        // a相の立ち上がりのみを見る
        _a.rise(this, &OriginalEncoderA::a_high_edge);
        // _a.fall(this, &OriginalEncoderA::a_low_edge);

    }

    void reset(){
        pulse_cnt = 0;
        cw = true;
    }

    int get_pulse_cnt(){
        return pulse_cnt;
    }

    bool get_cw(){
        return cw;
    }

private:
    void a_high_edge(){

        if(!_b) cw = 1;
        else cw = 0;

        pulse_cnting();
    }


    void pulse_cnting(){
        if(cw) pulse_cnt++;
        else pulse_cnt--;
    }

};

#endif
