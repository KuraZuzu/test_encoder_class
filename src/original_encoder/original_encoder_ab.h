#ifndef INCLUDED_ORIGINAL_ENCODER_AB
#define INCLUDED_ORIGINAL_ENCODER_AB

#include "mbed.h"

class OriginalEncoderAb {

private:
    InterruptIn _a;
    InterruptIn _b;
    bool a;
    bool b;
    bool cw; //回転方向 true<-正転、false<-逆転
    int pulse_cnt = 0; //回転数

public:

    OriginalEncoderAb(PinName a_pin, PinName b_pin) : _a(a_pin), _b(b_pin), pulse_cnt(0){

    }

    void start(){
        // a相とb相の立ち上がり・立ち下がりを検出してHIGH・LOWを記録
        _a.mode(PullNone);
        _b.mode(PullNone);
        _a.rise(this, &OriginalEncoderAb::a_high_edge);
        // _a.fall(this, &OriginalEncoderAb::a_low_edge);
        // _b.rise(this, &OriginalEncoderAb::b_high_edge);
        // _b.fall(this, &OriginalEncoderAb::b_low_edge);
    }

    void reset(){
        pulse_cnt = 0;
        cw = true;
    }

    int get_pulse_cnt(){
        return pulse_cnt;
    }

    bool get_cw(){
        return cw;
    }

private:
    void a_high_edge(){
        a = 1;

        if(a != b) cw = 1;
        else cw = 0;

        pulse_cnting();
    }

    void a_low_edge(){
        a = 0;

        if(a != b) cw = 1;
        else cw = 0;

        pulse_cnting();
    }

    void b_high_edge(){
        b = 1;

        if(a == b) cw = 0;
        else cw = 1;

        pulse_cnting();
    }

    void b_low_edge(){
        b = 0;

        if(a == b) cw = 0;
        else cw = 1;

        pulse_cnting();
    }

    void pulse_cnting(){
        if(cw) pulse_cnt++;
        else pulse_cnt--;
    }

};

#endif
